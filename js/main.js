$(document).ready(function() {

  var AccordionCategories = {
    openItem: function(item) {
      item.addClass("open");
      item.children(".sublist").slideDown(300);
    },
    closeItem: function(item) {
      item.removeClass("open");
      item.children(".sublist").slideUp(300);

      item.find(".item.open").each(function() {
        $(this).removeClass("open");
        $(this).children(".sublist").slideUp(300);
      });

      if (item.find(".title-fixed.show").length > 0) {
        $("html, body").animate({scrollTop: item.offset().top}, 200);
      }
    },
    openAll: function() {
      $(".categoryWrap .listWrap .item").each(function() {
        AccordionCategories.openItem($(this));
      });
    },
    closeAll: function() {
      $(".categoryWrap .listWrap > .list > .item").each(function() {
        AccordionCategories.closeItem($(this));
      });
    }
  }

  $(this)

  .on("click", ".categoryWrap .listWrap .item > .title", function() {
    var item = $(this).parent();

    if (item.hasClass("open")) {
      AccordionCategories.closeItem(item);
    }
    else {
      AccordionCategories.openItem(item);
    }
  })

  .on("click", ".categoryWrap .listWrap .toggleAll", function() {
    var move = $(this).data("move");

    if (move == 'open') {
      $(this).data("move", "close");
      $(this).text($(this).data("close-text"));
      AccordionCategories.openAll();
    }
    else {
      $(this).data("move", "open");
      $(this).text($(this).data("open-text"));
      AccordionCategories.closeAll();
    }

    return false;
  })

  .on("click", "header.header .mobileIcons [data-toggle]:not(.disabledHeaderOpen)", function() {
    var classElem = $(this).data("toggle");
    $("header.header ."+classElem).toggleClass("show");
  })

  .on("click", "body", function() {
    //
  });

  $(".mainSlider").each(function() {
    $(this).owlCarousel({
      singleItem: true,
  		items: 1,
  		pullDrag: false,
  		loop: true,
  		animateOut: 'fadeOut',
  		animateIn: 'fadeIn',
  		mouseDrag: false,
      autoplay: true,
      autoplayHoverPause: true,
      autoplaySpeed: 3000,
      nav: true,
      dotsClass: 'nav-dots container',
      dotClass: 'item',
      stageOuterClass: 'slides',
      navContainerClass: 'nav container',
      navClass: ['prev icon-arrow-left', 'next icon-arrow-right'],
      navElement: 'i',
      navText: ['', '']
    });
  });

  function setMinMain() {
    var headerHeight = $("header").length > 0 ? $("header").outerHeight() : 0;
    var footerHeight = $("footer").length > 0 ? $("footer").outerHeight() : 0;
    var windowHeight = $(window).height();

    $("main").css("min-height", windowHeight-headerHeight-footerHeight);
  }

  setMinMain();

  function fixedSectionAccordionCategories() {
    $(window).on("scroll", function() {
      var top = $(window).scrollTop();
      var topOpenSections = [];

      $(".categoryWrap .listWrap .list > .section").each(function() {
        topOpenSections[$(this).offset().top] = $(this);
      });

      var fixedTop = 0;
      var fixedSectionTitle = null;

      for (t in topOpenSections) {
        var tt = Number(t);
        var section = topOpenSections[t];
        var sectionTitleFixed = section.find(".title-fixed");
        var heightTitle = Number(sectionTitleFixed.innerHeight());
        if (top > tt-heightTitle && tt > fixedTop) {
          fixedTop = tt;
          if (top > tt && section.hasClass("open"))
            fixedSectionTitle = sectionTitleFixed;
          else
            fixedSectionTitle = null;
        }
      }

      var notShowTitles = $(".categoryWrap .listWrap .list > .section > .title-fixed");

      if (fixedSectionTitle) {
        notShowTitles = notShowTitles.not(fixedSectionTitle);
        fixedSectionTitle.addClass("show");
      }

      notShowTitles.removeClass("show");
    });
  }

  if ($(".categoryWrap .listWrap").length > 0) {
    fixedSectionAccordionCategories();
  }

  function fixedOtherCategories() {
    var block = $(".categoryWrap .otherCategories");
    var topBlock = Number(block.position().top);
    var leftBlock = Number(block.position().left);

    var prevScroll = 0;

    var topWrap = $(".categoryWrap").offset().top;
    var heightBlock = block.innerHeight();

    var emptyBottom = heightBlock < $(window).innerHeight() ? $(window).innerHeight() - heightBlock : 0;

    $(".categoryWrap").css("min-height", $(".categoryWrap").innerHeight());

    block.css({
      width: block.innerWidth(),
      position: 'absolute',
      top: 0,
      right: 0
    });

    $(window).on("scroll", function() {
      var top = $(window).scrollTop();

      if (top > prevScroll) {
        var underBottom = $(window).innerHeight()+top-(emptyBottom > 0 ? -10 : 20)-topWrap-heightBlock-emptyBottom;
        var currentBlockBottom = $(".categoryWrap").innerHeight() - block.position().top - heightBlock;
        if (underBottom > 0) {
          if (currentBlockBottom > 0) {
            block.css({
              top: underBottom,
            });
          }
          else {
            block.css({
              top: $(".categoryWrap").innerHeight() - heightBlock
            });
          }
        }
        else {
          block.css({
            top: 0
          });
        }
      }
      else {
        var underTop = block.offset().top - top;
        var currentBlockTop = block.position().top;
        if (currentBlockTop > 0) {
          if (underTop > 0) {
            block.css({
              top: currentBlockTop - underTop,
            });
          }

          var currentBlockBottom = $(".categoryWrap").innerHeight() - block.position().top - heightBlock;
          if (currentBlockBottom < 0) {
            block.css({
              top: $(".categoryWrap").innerHeight() - heightBlock
            });
          }
        }
        else {
          block.css({
            top: 0
          });
        }
      }

      prevScroll = top;
    });
  }

  if ($(".categoryWrap .otherCategories").length > 0 && $(window).innerWidth() > 480) {
    fixedOtherCategories();
  }

  $('.svg').each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(imgURL, function(data) {
				var $svg = $(data).find('svg');

				if (typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				if (typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass+' replaced-svg');
				}

				$svg = $svg.removeAttr('xmlns:a');
				$svg.removeClass("svg");

				if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
					$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
				}

				$img.replaceWith($svg);
				$svg.show();
		}, 'xml');

	});

});
